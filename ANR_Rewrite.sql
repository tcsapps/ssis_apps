﻿USE [c2000]
GO

/****** Object:  StoredProcedure [dbo].[usp_report_ANR_registered_details]    Script Date: 3/2/2022 1:54:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Swathi Tallapalli
-- Create date: 2022-02-28
-- Description:	This stored procedure is to identify degree/program level registered and not registered details for given term in SAYBROOK University.
-- =============================================

--CREATE PROCEDURE [tcs].[usp_report_ANR_registered_details]
ALTER PROCEDURE [dbo].[usp_report_ANR_registered_details] --change the schema (Commented out by Zach Roy on 3-1-2022)
( 
 @Terms int,
 @Campus int
)

AS
BEGIN

select  A.Degree_Level 
      , A.program_version_description
      , A.EnrollmentSySchoolStatus
      , A.Term
      , A.anr
      , A.CampusCode
      , A.IsRegisteredForClass
      , sum(CASE A.IsRegisteredForClass WHEN 1 THEN A.credithours ELSE 0 END) as reg_credithours --Only need to sum the credit hours if the student is actually registered for the classes.
      --, sum(A.credithours) as reg_credithours --(Commented out by Zach Roy on 3-1-2022)
      , count(DISTINCT A.SyStudentID) as reg_student --Need to count the distint student records, otherwise it will be a count of courses that are registered

from (select deg.Descrip as Degree_Level, 
	           prog.Descrip as program_version_description, 
	           sc.Descrip as EnrollmentSySchoolStatus,
	           adt.Descrip as Term,
	           enr.AdEnrollID,
             enrs.Credits as credithours,
             enr.SyStudentID , 
             0 as anr,
             --anr.IsActiveEnrollment,
             syc.Code as CampusCode,
             CASE
               WHEN EXISTS (SELECT 1
                            FROM c2000.dbo.AdEnrollSched x
                            WHERE x.AdEnrollID = enr.AdenrollID
                              AND x.STATUS IN ('S','C','P') -- If A student is has courses in status 'Scheduled', 'Current', 'Completed', they are considered registered during that term                         
                              AND (x.IsAudit = 0 OR x.IsAudit IS NULL)
                              AND x.adtermID IN (@Terms)
                              AND x.AdCourseID <> 37496  --Saybrook CONCRED course
                            ) THEN 1
		             ELSE 0
	           END AS [IsRegisteredForClass]
      from dbo.AdDegree as deg
        inner join dbo.AdProgramVersion as prog on deg.AdDegreeID      = prog.AdDegreeID
        inner join dbo.AdEnroll         as enr  on enr.AdProgramID     = prog.AdProgramID
        inner join dbo.syschoolstatus   as sc   on sc.syschoolstatusid = enr.syschoolstatusid
        inner join dbo.AdEnrollSched    as enrs on enrs.AdEnrollID     = enr.AdEnrollID
        inner join dbo.AdTerm           as adt  on adt.AdTermID        = enrs.AdTermID
        inner join dbo.SyCampus         as syc  on syc.SyCampusID      = enr.SyCampusID
        CROSS APPLY [tcs].[cst_Check_Student_Enroll_For_TCSActiveStatus] (enr.AdEnrollID) active --use this to get the active students
      where active.IsActiveEnrollment = 1  --instead of hardcoding the status filter, use the function
      --where sc.SyschoolStatusID in (5,13)  --use the function to filter to active statuses (Commented out by Zach Roy on 3-1-2022)
                                             --potentially parameterize the syschoolstatusids if they want to see different slices
        and syc.SyCampusID=@Campus
        and enrs.adTermID in ( @Terms) --filter on the AdTermID on the AdEnrollSched table so that we are looking at the course schedules for the @Terms paramter

           ) A
group by A.Degree_Level,
         A.program_version_description, 
         A.EnrollmentSySchoolStatus, 
         A.Term,
         A.anr, 
         A.CampusCode,
         A.IsRegisteredForClass

END
GO


